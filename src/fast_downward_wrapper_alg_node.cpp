#include "fast_downward_wrapper_alg_node.h"

FastDownwardWrapperAlgNode::FastDownwardWrapperAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<FastDownwardWrapperAlgorithm>()
{
  //init class attributes if necessary
  this->loop_rate_ = 2;//in [Hz]

  std::string fast_downward_folder, working_folder, heuristic, search_method;
  bool write_domain = false;

  this->public_node_handle_.param("fast_downward_folder",fast_downward_folder,FAST_DOWNWARD_FOLDER);
  this->public_node_handle_.param("problem_name",this->problem_name,PROBLEM_NAME);
  this->public_node_handle_.param("domain_name",this->domain_name,DOMAIN_NAME);
  this->public_node_handle_.param("working_folder",working_folder,WORKING_FOLDER);
  this->public_node_handle_.param("heuristic",heuristic,HEURISTIC);
  this->public_node_handle_.param("search_method",search_method,SEARCH_METHOD);

  std::cout << "\niri_fast_downward_wrapper set up:\n" 
            << "fast_downward_folder: " << fast_downward_folder << "\n"
            << "domain_name: " << domain_name << "\n"
            << "problem_name: " << problem_name << "\n"
            << "working_folder: " << working_folder << "\n"
            << "heuristic: " << heuristic << "\n"
            << "search_method: " << search_method << "\n\n";

  ROS_INFO("Screen On ");

  this->alg_.setFastDownwardFolder(fast_downward_folder);
  this->alg_.setWorkingFolder(working_folder);
  
  

  this->alg_.setHeuristic(heuristic);
  this->alg_.setSearch(search_method);

  // [init publishers]
  
  // [init subscribers]
  
  // [init services]
  this->get_complete_fast_downward_plan_server_ = this->public_node_handle_.advertiseService("/get_complete_fast_downward_plan", &FastDownwardWrapperAlgNode::get_complete_fast_downward_planCallback, this);
  pthread_mutex_init(&this->get_complete_fast_downward_plan_mutex_,NULL);

  this->get_fast_downward_plan_server_ = this->public_node_handle_.advertiseService("/get_fast_downward_plan", &FastDownwardWrapperAlgNode::get_fast_downward_planCallback, this);
  pthread_mutex_init(&this->get_fast_downward_plan_mutex_,NULL);

  
  // [init clients]
  
  // [init action servers]
  
  // [init action clients]
}

FastDownwardWrapperAlgNode::~FastDownwardWrapperAlgNode(void)
{
  // [free dynamic memory]
  pthread_mutex_destroy(&this->get_complete_fast_downward_plan_mutex_);
  pthread_mutex_destroy(&this->get_fast_downward_plan_mutex_);
}

void FastDownwardWrapperAlgNode::mainNodeThread(void)
{
  // [fill msg structures]
  
  // [fill srv structure and make request to the server]
  
  // [fill action structure and make request to the action server]

  // [publish messages]
}

/*  [subscriber callbacks] */

/*  [service callbacks] */
bool FastDownwardWrapperAlgNode::get_complete_fast_downward_planCallback(iri_fast_downward_wrapper::FastDownwardCompletePlan::Request &req, iri_fast_downward_wrapper::FastDownwardCompletePlan::Response &res)
{
  ROS_INFO("FastDownwardWrapperAlgNode::get_complete_fast_downward_planCallback: New Request Received!");

  //use appropiate mutex to shared variables if necessary
  //this->alg_.lock();
  //this->get_complete_fast_downward_plan_mutex_enter();

  
  // ----- CONVERSIONS ----------
  
  // domain predicates
  Fast_Downward_Wrapper::DomainSymbolicPredicates domain_symbolic_predicates;
  domain_symbolic_predicates.resize(req.domain_symbolic_predicates.size());
  for (uint i = 0; i < domain_symbolic_predicates.size(); ++i)
  {
    domain_symbolic_predicates[i].name = req.domain_symbolic_predicates[i].name;
    domain_symbolic_predicates[i].variables = req.domain_symbolic_predicates[i].objects;
    domain_symbolic_predicates[i].types = req.domain_symbolic_predicates[i].types;
  }

  // actions
  Fast_Downward_Wrapper::DomainActions domain_actions;
  domain_actions.resize(req.actions.size());
  for (uint i = 0; i < domain_actions.size(); ++i)
  {
    domain_actions[i].action_name = req.actions[i].name;
    domain_actions[i].parameters.params = req.actions[i].parameters;
    domain_actions[i].parameters.types = req.actions[i].parameters_types;
    domain_actions[i].preconditions = req.actions[i].preconditions;
    domain_actions[i].effects = req.actions[i].effects;
    domain_actions[i].cost = req.actions[i].cost;  
  }

  this->alg_.setDomainPDDLFile(this->domain_name + "_full");
  this->alg_.setProblemPDDLFile(this->problem_name + "_full");

  this->alg_.addTotalCost(req.add_total_cost);
  this->alg_.setTypes(req.types);
  this->alg_.setDomainSymbolicPredicates(domain_symbolic_predicates);
  this->alg_.setDomainActions(domain_actions);
  this->alg_.writeDomainPDDLFile();

  //write the pddl and get the plan
  iri_fast_downward_wrapper::FastDownwardPlan::Request req_;
  req_.objects = req.objects;
  req_.symbolic_predicates = req.symbolic_predicates;
  req_.goal = req.goal;
  iri_fast_downward_wrapper::FastDownwardPlan::Response res_;
  writing_domain = true;
  if(this->get_fast_downward_planCallback(req_,res_))
  {
    res.plan = res_.plan;
    res.success = res_.success;
    writing_domain = false;
    return true;
  }

  return false;

  //ROS_INFO("FastDownwardWrapperAlgNode::get_complete_fast_downward_planCallback: Processing New Request!");
  //do operations with req and output on res
  //res.data2 = req.data1 + my_var;

  //unlock previously blocked shared variables
  //this->get_complete_fast_downward_plan_mutex_exit();
  //this->alg_.unlock();

  //return true;
}

void FastDownwardWrapperAlgNode::get_complete_fast_downward_plan_mutex_enter(void)
{
  pthread_mutex_lock(&this->get_complete_fast_downward_plan_mutex_);
}

void FastDownwardWrapperAlgNode::get_complete_fast_downward_plan_mutex_exit(void)
{
  pthread_mutex_unlock(&this->get_complete_fast_downward_plan_mutex_);
}

bool FastDownwardWrapperAlgNode::get_fast_downward_planCallback(iri_fast_downward_wrapper::FastDownwardPlan::Request &req, iri_fast_downward_wrapper::FastDownwardPlan::Response &res)
{
  ROS_INFO("FastDownwardWrapperAlgNode::get_fast_downward_planCallback: New Request Received!");

  if(writing_domain)
  {
    this->alg_.setDomainPDDLFile(this->domain_name + "_full");
    this->alg_.setProblemPDDLFile(this->problem_name + "_full");
  }
  else
  {
    this->alg_.setDomainPDDLFile(this->domain_name);
    this->alg_.setProblemPDDLFile(this->problem_name);
  }

  //use appropiate mutex to shared variables if necessary
  //this->alg_.lock();
  //this->get_fast_downward_plan_mutex_enter();

  //ROS_INFO("FastDownwardWrapperAlgNode::get_fast_downward_planCallback: Processing New Request!");

  Fast_Downward_Wrapper::Objects objects;
  // do the conversion from the msg to the Fast_Downward_Wrapper::Objects struct
  objects.resize(req.objects.size());

  if(req.objects.size() == 0)
  {
    ROS_ERROR("No objects specified - Exciting");
    return false;
  }

  for (int i = 0; i < req.objects.size(); ++i)
  {
    objects[i].object = req.objects[i].object_name;
    objects[i].type = req.objects[i].type;
  }
  this->alg_.setObjects(objects);

  if(req.symbolic_predicates.size() == 0)
  {
    ROS_ERROR("No predicates specified - Keep executing");
  }
  
  // Symbolic Predicates
  Fast_Downward_Wrapper::SymbolicPredicates sym_predicates;
  sym_predicates.resize(req.symbolic_predicates.size());
  for (int i = 0; i < req.symbolic_predicates.size(); ++i)
  {
    sym_predicates[i].name = req.symbolic_predicates[i].predicate_name;
    sym_predicates[i].objects.resize(req.symbolic_predicates[i].objects.size());
    for (int o = 0; o < req.symbolic_predicates[i].objects.size(); ++o)
    {
      sym_predicates[i].objects[o] = req.symbolic_predicates[i].objects[o];
    }
  }
  this->alg_.setSymbolicPredicates(sym_predicates);

  // set goal
  this->alg_.setGoal(req.goal);

  // compute the plan
  res.success = true;
  if(!this->alg_.computePlan())
  {
    ROS_ERROR("THERE IS NO SOLUTION FOR THE DEFINED PROBLEM");
    res.success = false;
    return false;
  }
  

  
  // get the plan and convert it to the message
  Fast_Downward_Wrapper::Plan plan = this->alg_.getPlan();
  iri_fast_downward_wrapper::Plan plan_msg;
  plan_msg.cost = plan.cost;
  plan_msg.actions.resize(plan.actions.size());
  for (int i = 0; i < plan.actions.size(); ++i)
  {
    plan_msg.actions[i].action_name = plan.actions[i].action;
    plan_msg.actions[i].objects.resize(plan.actions[i].objects.size());
    for (int o = 0; o < plan.actions[i].objects.size(); ++o)
    {
      plan_msg.actions[i].objects[o] = plan.actions[i].objects[o];
    }
  }
  res.plan = plan_msg;

  //print the plan
  std::cout << "\n The plan is:\n";
  this->alg_.printPlan();

  //unlock previously blocked shared variables
  //this->get_fast_downward_plan_mutex_exit();
  //this->alg_.unlock();

  return true;
}

void FastDownwardWrapperAlgNode::get_fast_downward_plan_mutex_enter(void)
{
  pthread_mutex_lock(&this->get_fast_downward_plan_mutex_);
}

void FastDownwardWrapperAlgNode::get_fast_downward_plan_mutex_exit(void)
{
  pthread_mutex_unlock(&this->get_fast_downward_plan_mutex_);
}


/*  [action callbacks] */

/*  [action requests] */

void FastDownwardWrapperAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();
  this->config_=config;
  this->alg_.unlock();
}

void FastDownwardWrapperAlgNode::addNodeDiagnostics(void)
{
}

/* main function */
int main(int argc,char *argv[])
{
  return algorithm_base::main<FastDownwardWrapperAlgNode>(argc, argv, "fast_downward_wrapper_alg_node");
}
