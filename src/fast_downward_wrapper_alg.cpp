#include "fast_downward_wrapper_alg.h"
#include "iri_fast_downward_wrapper/Plan.h"

FastDownwardWrapperAlgorithm::FastDownwardWrapperAlgorithm(void)
{
  pthread_mutex_init(&this->access_,NULL);
}

FastDownwardWrapperAlgorithm::~FastDownwardWrapperAlgorithm(void)
{
  pthread_mutex_destroy(&this->access_);
}

void FastDownwardWrapperAlgorithm::config_update(Config& config, uint32_t level)
{
  this->lock();

  // save the current configuration
  this->config_=config;
  
  this->unlock();
}

// FastDownwardWrapperAlgorithm Public API
void FastDownwardWrapperAlgorithm::setFastDownwardFolder(std::string planner_folder)
{
	this->fdw.setFastDownwardFolder(planner_folder);
}
void FastDownwardWrapperAlgorithm::setWorkingFolder(std::string working_folder)
{
	// BE CAREFULL WITH THISSSS ----------------
	this->fdw.setCurrentFolder(working_folder);
	//------------------------------------------

	this->fdw.setProblemFolder(working_folder);
	this->fdw.setDomainFolder(working_folder);
}

void FastDownwardWrapperAlgorithm::setDomainPDDLFile(std::string domain_file_name)
{
	this->fdw.setDomainPDDLFile(domain_file_name);
}

void FastDownwardWrapperAlgorithm::setProblemPDDLFile(std::string problem_file_name)
{
	this->fdw.setProblemPDDLFile(problem_file_name);
}

void FastDownwardWrapperAlgorithm::setObjects(Fast_Downward_Wrapper::Objects objects)
{
	this->fdw.setObjects(objects);
}

void FastDownwardWrapperAlgorithm::setSymbolicPredicates(Fast_Downward_Wrapper::SymbolicPredicates sym_predicates)
{
	this->fdw.setSymbolicPredicates(sym_predicates);
}


void FastDownwardWrapperAlgorithm::setGoal(std::string goal)
{
	this->fdw.setGoal(goal);
}


void FastDownwardWrapperAlgorithm::setHeuristic(std::string heuristic)
{
	this->fdw.setHeuristic(heuristic);
}


void FastDownwardWrapperAlgorithm::setSearch(std::string search)
{
	this->fdw.setSearch(search);
}


bool FastDownwardWrapperAlgorithm::computePlan()
{
	this->fdw.writeProblemPDDLFile();
	return this->fdw.computePlan();
}


void FastDownwardWrapperAlgorithm::printPlan()
{
	this->fdw.printPlan();
}

Fast_Downward_Wrapper::Plan FastDownwardWrapperAlgorithm::getPlan()
{
	return this->fdw.getPlan();
}

void FastDownwardWrapperAlgorithm::addTotalCost(bool cost)
{
	if(cost)
		this->fdw.addTotalCost();
	return;
}

void FastDownwardWrapperAlgorithm::setTypes(std::vector<std::string> types)
{
	this->fdw.setTypes(types);
}

void FastDownwardWrapperAlgorithm::setDomainSymbolicPredicates
(Fast_Downward_Wrapper::DomainSymbolicPredicates domain_symbolic_predicates)
{
	this->fdw.setDomainPredicates(domain_symbolic_predicates);
}

void FastDownwardWrapperAlgorithm::setDomainActions(Fast_Downward_Wrapper::DomainActions domain_actions)
{
	this->fdw.setDomainActions(domain_actions);
}

void FastDownwardWrapperAlgorithm::writeDomainPDDLFile()
{
	this->fdw.writeDomainPDDLFile();
}
