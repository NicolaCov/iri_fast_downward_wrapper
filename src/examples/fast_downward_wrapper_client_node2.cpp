/**
 * Example:
 * 
 * Resolve this problem, where two objects are touching and the goal is to grasp all of them.
 * We know suppose that we have only two grasping actions,
 * one grasping action per object with differents costs. In this example we are going to 
 * write the domain pddl file. 
 * 
 * The actions are:
 * grasp-o1
 * grasp-o2
 *  ____
 * |   |______
 * | o0|      |  
 * |   |__o1__|
 * |___|
 * 
 * 
 * To run the code launch in two different terminals the following commands:
 * \code
 * $ roslaunch iri_fast_downward_wrapper fast_downward_server_example.launch 
 * $ rosrun iri_fast_downward_wrapper iri_fast_downward_wrapr_client_example2
 * \endcode
 * We will see that the plan is feasible, and on the terminal of the server
 * we will see the plan printed:
 * \code
 * The plan is:
 * Action: grasp-o0  with parameters:  
 * Action: grasp-o1  with parameters:  
 * Plan with cost: 15
 * \endcode 
 * 
 */

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"
#include <string>

#include "iri_fast_downward_wrapper/FastDownwardCompletePlan.h"

// this is included only beause in that library there are some 
// methods that allow to build quickly the precondtions, effects and name
// of the actions for the framework of my Thesis. 
#include <fast_downward_wrapper.h> 


int main(int argc, char **argv)
{

  ros::init(argc, argv, "iri_fast_downward_client_example2");

  ros::NodeHandle n("~");

  std::string service_name = "/get_complete_fast_downward_plan";

  ros::service::waitForService(service_name,5);// 5 seconds

  ros::ServiceClient client = n.serviceClient<iri_fast_downward_wrapper::FastDownwardCompletePlan>(service_name);
  
  // create the service and fill it
  iri_fast_downward_wrapper::FastDownwardCompletePlan srv;

  srv.request.objects.resize(2);
  srv.request.objects[0].object_name = "o0";
  srv.request.objects[0].type = "obj";
  srv.request.objects[1].object_name = "o1";
  srv.request.objects[1].type = "obj";

  srv.request.symbolic_predicates.resize(4);
  srv.request.symbolic_predicates[0].predicate_name = "block_dir3";
  srv.request.symbolic_predicates[0].objects.push_back("o1");
  srv.request.symbolic_predicates[0].objects.push_back("o0");

  srv.request.symbolic_predicates[1].predicate_name = "block_dir4";
  srv.request.symbolic_predicates[1].objects.push_back("o1");
  srv.request.symbolic_predicates[1].objects.push_back("o0");

  srv.request.symbolic_predicates[2].predicate_name = "block_dir1";
  srv.request.symbolic_predicates[2].objects.push_back("o0");
  srv.request.symbolic_predicates[2].objects.push_back("o1");

  srv.request.symbolic_predicates[3].predicate_name = "block_dir2";
  srv.request.symbolic_predicates[3].objects.push_back("o0");
  srv.request.symbolic_predicates[3].objects.push_back("o1");

  //we want the obj o1 to be separated from all the others (that is from o0)
  srv.request.goal ="(and\
                     (not (exists (?x - obj)(not (removed ?x))))\n\
                     )";

  srv.request.add_total_cost = true;
  srv.request.types.push_back("obj");
  srv.request.types.push_back("direction");
  
  // domain predicates
  srv.request.domain_symbolic_predicates.resize(6);
  srv.request.domain_symbolic_predicates[0].name = "block_dir";
  srv.request.domain_symbolic_predicates[0].objects.resize(3);
  srv.request.domain_symbolic_predicates[0].objects[0] = "o1";
  srv.request.domain_symbolic_predicates[0].objects[1] = "o2";
  srv.request.domain_symbolic_predicates[0].objects[2] = "d";
  srv.request.domain_symbolic_predicates[0].types.resize(3);
  srv.request.domain_symbolic_predicates[0].types[0] = "obj";
  srv.request.domain_symbolic_predicates[0].types[1] = "obj";
  srv.request.domain_symbolic_predicates[0].types[2] = "direction";
  
  srv.request.domain_symbolic_predicates[1].name = "on";
  srv.request.domain_symbolic_predicates[1].objects.resize(2);
  srv.request.domain_symbolic_predicates[1].objects[0] = "o1";
  srv.request.domain_symbolic_predicates[1].objects[1] = "o2";
  srv.request.domain_symbolic_predicates[1].types.resize(2);
  srv.request.domain_symbolic_predicates[1].types[0] = "obj";
  srv.request.domain_symbolic_predicates[1].types[1] = "obj";
  
  srv.request.domain_symbolic_predicates[2] = srv.request.domain_symbolic_predicates[1];
  srv.request.domain_symbolic_predicates[2].name = "block_grasp";

  srv.request.domain_symbolic_predicates[3].name = "removed";
  srv.request.domain_symbolic_predicates[3].objects.resize(1);
  srv.request.domain_symbolic_predicates[3].objects[0] = "o";
  srv.request.domain_symbolic_predicates[3].types.push_back("obj");

  srv.request.domain_symbolic_predicates[4].name = "ik_unfeasible_dir";
  srv.request.domain_symbolic_predicates[4].objects.resize(2);
  srv.request.domain_symbolic_predicates[4].objects[0] = "o";
  srv.request.domain_symbolic_predicates[4].objects[1] = "d";
  srv.request.domain_symbolic_predicates[4].types.resize(2);
  srv.request.domain_symbolic_predicates[4].types[0] = "obj";
  srv.request.domain_symbolic_predicates[4].types[1] = "direction";

  srv.request.domain_symbolic_predicates[5].name = "ik_unfeasible_grasp";
  srv.request.domain_symbolic_predicates[5].objects.resize(1);
  srv.request.domain_symbolic_predicates[5].objects[0] = "o";
  srv.request.domain_symbolic_predicates[5].types.push_back("obj");

  // domain actions
  CFast_Downward_Wrapper fdw;
  srv.request.actions.resize(2);
  srv.request.actions[0].name = fdw.getGraspActionName(0);
  srv.request.actions[0].preconditions = fdw.getActionGraspPrecondition("o0");
  srv.request.actions[0].effects = fdw.getActionGraspEffect("o0", 10);
  
  srv.request.actions[1].name = fdw.getGraspActionName(1);
  srv.request.actions[1].preconditions = fdw.getActionGraspPrecondition("o1");
  srv.request.actions[1].effects = fdw.getActionGraspEffect("o1", 5); // the second parameter is the cost

  ROS_INFO("Calling the plan wrapper service");
  if (client.call(srv))
  {
    ROS_INFO("Feasible Plan - Got a plan");
  }
  else
  {
    ROS_ERROR("Not Feasible Plan");
  }

  ros::spin();

  return 0;
}