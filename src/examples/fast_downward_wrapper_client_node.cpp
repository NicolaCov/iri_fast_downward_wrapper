/**
 * Example:
 * 
 * Resolve this problem, where two objects are touching and the goal is to 
 * make o0 to have no near objects.
 *  ____
 * |   |______
 * | o0|      |  
 * |   |__o1__|
 * |___|
 * 
 * To run the code launch in two different terminals the following commands:
 * \code
 * $ roslaunch iri_fast_downward_wrapper fast_downward_server_example.launch 
 * $ rosrun iri_fast_downward_wrapper iri_fast_downward_wrapr_client_example2
 * \endcode
 */

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"
#include <string>

#include "iri_fast_downward_wrapper/FastDownwardPlan.h"


int main(int argc, char **argv)
{

  ros::init(argc, argv, "iri_fast_downward_client_example");

  ros::NodeHandle n("~");

  std::string service_name = "/get_fast_downward_plan";

  ros::service::waitForService(service_name,5);// 5 seconds

  ros::ServiceClient client = n.serviceClient<iri_fast_downward_wrapper::FastDownwardPlan>(service_name);
  

  // create the service and fill it
  iri_fast_downward_wrapper::FastDownwardPlan srv;

  srv.request.objects.resize(2);
  srv.request.objects[0].object_name = "o0";
  srv.request.objects[0].type = "obj";
  srv.request.objects[1].object_name = "o1";
  srv.request.objects[1].type = "obj";

  srv.request.symbolic_predicates.resize(4);
  srv.request.symbolic_predicates[0].predicate_name = "block_dir3";
  srv.request.symbolic_predicates[0].objects.push_back("o1");
  srv.request.symbolic_predicates[0].objects.push_back("o0");

  srv.request.symbolic_predicates[1].predicate_name = "block_dir4";
  srv.request.symbolic_predicates[1].objects.push_back("o1");
  srv.request.symbolic_predicates[1].objects.push_back("o0");

  srv.request.symbolic_predicates[2].predicate_name = "block_dir1";
  srv.request.symbolic_predicates[2].objects.push_back("o0");
  srv.request.symbolic_predicates[2].objects.push_back("o1");

  srv.request.symbolic_predicates[3].predicate_name = "block_dir2";
  srv.request.symbolic_predicates[3].objects.push_back("o0");
  srv.request.symbolic_predicates[3].objects.push_back("o1");

  //we want the obj o1 to be separated from all the others (that is from o0)
  srv.request.goal ="(and\
                     (not (exists (?x - obj)(block_dir1 ?x o1)))\n\
                     (not (exists (?x - obj)(block_dir2 ?x o1)))\n\
                     (not (exists (?x - obj)(block_dir3 ?x o1)))\n\
                     (not (exists (?x - obj)(block_dir4 ?x o1)))\n\
                     )";

  // srv.request.goal = "(grasped o1)";

  ROS_INFO("Calling the plan wrapper service");
  if (client.call(srv))
  {
    ROS_INFO("Feasible Plan - Got a plan");
  }
  else
  {
    ROS_ERROR("Not Feasible Plan");
  }

  ros::spin();

  return 0;
}