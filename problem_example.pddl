(define (problem problem_example)
(:domain domain_example)
(:objects 
o0 - obj 
o1 - obj 
)
(:init 
(block_dir3 o1 o0 )
(block_dir4 o1 o0 )
(block_dir1 o0 o1 )
(block_dir2 o0 o1 )
)
(:goal 
(and                     (not (exists (?x - obj)(block_dir1 ?x o1)))
                     (not (exists (?x - obj)(block_dir2 ?x o1)))
                     (not (exists (?x - obj)(block_dir3 ?x o1)))
                     (not (exists (?x - obj)(block_dir4 ?x o1)))
                     )
)
)