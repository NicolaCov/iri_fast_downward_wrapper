(define (domain domain_)

	;; In this domain we consider both pushing and grasping actions 
	(:requirements :adl :existential-preconditions :universal-preconditions)
	(:types obj)
	(:predicates  
				
		; in this framework we are considerig that the object can be pushed in 4 direction
		; - 2 along the principal axis 
		; - 2 along the axis orthogonal to the principal one
		; An object B can be block another one in two ways:
		; -1) because the object A that will be moved will collide with B, so it block 
		;     the movement of A along the considered direction
		; -2) because the end effector of the robot will collide with it 
	  ;     the movement of A along the considered direction
	  (block_dir1 ?o1 ?o2 - obj) ;; true if ?o1 blocks ?o2 in the north direction
		(block_dir2 ?o1 ?o2 - obj) ;; true if ?o1 blocks ?o2 in the south direction
		(block_dir3 ?o1 ?o2 - obj) ;; true if ?o1 blocks ?o2 in the est direction
		(block_dir4 ?o1 ?o2 - obj) ;; true if ?o1 blocks ?o2 in the west direction

		(on ?o1 ?o2 - obj) ;; true if ?o1 is on ?o2
		(clear ?o - obj) ;; if no objects are on ?o this probably can substitude by considering
										 ;; only the "on" predicates and the "exists" instruction
		(graspable ?o - obj) ;; true if the object ?o can be grasped
		(block_grasp ?o1 ?o2 - obj) ;; true if ?o1 blocks ?o2 to be grasped

		(grasped ?o - obj)  ;; true if ?o has been grasped, this is the predicate that specify
									;; is no longer in the scene

		; predicates for the inverse kinematic, an object cannot be pushed along a certain direction
		; if the robot cannot perform the pushing path
		(ik_unfeasible_dir1 ?o - obj)
		(ik_unfeasible_dir2 ?o - obj)
		(ik_unfeasible_dir3 ?o - obj)
		(ik_unfeasible_dir4 ?o - obj)
		(ik_unfeasible_grasp ?o - obj)
	)

	;; push along direction 1
	(:action push_dir1
		:parameters (?o - obj)
		:precondition 	(and	
				 			
				 			;; if there no exist objects that block ?o in all the directions
				 			(not (and
				 			(exists (?x - obj)(block_dir1 ?x ?o))
				 			;(exists (?x - obj)(block_dir2 ?x ?o))
				 			;(exists (?x - obj)(block_dir3 ?x ?o))
				 			;(exists (?x - obj)(block_dir4 ?x ?o))
				 			))

				 			;;if there are no objects above it
				 			(not (exists (?x - obj)(on ?x ?o)))

				 			;;if the object is not above to anyother
				 			(not (exists (?x - obj)(on ?o ?x))) ; this could be easily done adding a predicate to say if an object is on the table or not.

				 			(not (ik_unfeasible_dir1 ?o))
						)
			     
		;; effects: free "o" and take off all the block predicates that have 
		;; something to do with it. Also free all the object that have no more blocking objects  
		:effect (and
			    	
				    (forall (?x - obj)
				    	(and

				    	;;; eliminate the blocking predicate to all the objects that are adjacent to "o"	
				    	(when (block_dir1 ?o ?x) (not (block_dir1 ?o ?x)))
					    (when (block_dir2 ?o ?x) (not (block_dir2 ?o ?x)))
					    (when (block_dir3 ?o ?x) (not (block_dir3 ?o ?x)))
					    (when (block_dir4 ?o ?x) (not (block_dir4 ?o ?x)))

					    ; eliminate all the others blocking conditions - the others objects blocking condition on ?o
					    (when (block_dir1 ?x ?o) (not (block_dir1 ?x ?o)))
					    (when (block_dir2 ?x ?o) (not (block_dir2 ?x ?o)))
					    (when (block_dir3 ?x ?o) (not (block_dir3 ?x ?o)))
					    (when (block_dir4 ?x ?o) (not (block_dir4 ?x ?o)))

	  					; update the block_grasp predicates of all the objects 
	  					; which cannot be grasped because of ?o 
 	  					(when (block_grasp ?o ?x) (not (block_grasp ?o ?x)))
 	  					(when (block_grasp ?x ?o) (not (block_grasp ?x ?o)))
 	  					
 	  					; update the ik_unfeasible_grasp, we consider that after a pushing
 	  					; if the object before couldn't be grasped, now it :constants
						(when (ik_unfeasible_grasp ?o) (not (ik_unfeasible_grasp ?o))) 	  					
					    )
				    )	   

	  				; remove all the geometric constraints
	  				(forall (?x - obj)
	  					(and
						(when (ik_unfeasible_dir1 ?x)(not (ik_unfeasible_dir1 ?x)))
		                (when (ik_unfeasible_dir2 ?x)(not (ik_unfeasible_dir2 ?x)))
		                (when (ik_unfeasible_dir3 ?x)(not (ik_unfeasible_dir3 ?x)))
		                (when (ik_unfeasible_dir4 ?x)(not (ik_unfeasible_dir4 ?x)))
		                (when (ik_unfeasible_grasp ?x)(not (ik_unfeasible_grasp ?x)))
	                	)
	                ) 
			    )	
	)

	;; push along direction 2
	(:action push_dir2
		:parameters (?o - obj)
		:precondition 	(and	
				 			
				 			;; if there no exist objects that block ?o in all the directions
				 			(not (and
				 			;(exists (?x - obj)(block_dir1 ?x ?o))
				 			(exists (?x - obj)(block_dir2 ?x ?o))
				 			;(exists (?x - obj)(block_dir3 ?x ?o))
				 			;(exists (?x - obj)(block_dir4 ?x ?o))
				 			))

				 			;;if there are no objects above it
				 			(not (exists (?x - obj)(on ?x ?o)))

				 			;;if the object is not above to anyother
				 			(not (exists (?x - obj)(on ?o ?x))) ; this could be easily done adding a predicate to say if an object is on the table or not.

				 			(not (ik_unfeasible_dir2 ?o))
						)
			     
		;; effects: free "o" and take off all the block predicates that have 
		;; something to do with it. Also free all the object that have no more blocking objects  
		:effect (and
			    	
				    (forall (?x - obj)
				    	(and

				    	;;; eliminate the blocking predicate to all the objects that are adjacent to "o"	
				    	(when (block_dir1 ?o ?x) (not (block_dir1 ?o ?x)))
					    (when (block_dir2 ?o ?x) (not (block_dir2 ?o ?x)))
					    (when (block_dir3 ?o ?x) (not (block_dir3 ?o ?x)))
					    (when (block_dir4 ?o ?x) (not (block_dir4 ?o ?x)))

					    ; eliminate all the others blocking conditions - the others objects blocking condition on ?o
					    (when (block_dir1 ?x ?o) (not (block_dir1 ?x ?o)))
					    (when (block_dir2 ?x ?o) (not (block_dir2 ?x ?o)))
					    (when (block_dir3 ?x ?o) (not (block_dir3 ?x ?o)))
					    (when (block_dir4 ?x ?o) (not (block_dir4 ?x ?o)))

	  					; update the block_grasp predicates of all the objects 
	  					; which cannot be grasped because of ?o 
 	  					(when (block_grasp ?o ?x) (not (block_grasp ?o ?x)))
 	  					(when (block_grasp ?x ?o) (not (block_grasp ?x ?o)))

 	  					; update the ik_unfeasible_grasp, we consider that after a pushing
 	  					; if the object before couldn't be grasped, now it :constants
						(when (ik_unfeasible_grasp ?o) (not (ik_unfeasible_grasp ?o))) 	  					
					    )
				    )

	  				; remove all the geometric constraints
	  				(forall (?x - obj)
	  					(and
						(when (ik_unfeasible_dir1 ?x)(not (ik_unfeasible_dir1 ?x)))
		                (when (ik_unfeasible_dir2 ?x)(not (ik_unfeasible_dir2 ?x)))
		                (when (ik_unfeasible_dir3 ?x)(not (ik_unfeasible_dir3 ?x)))
		                (when (ik_unfeasible_dir4 ?x)(not (ik_unfeasible_dir4 ?x)))
		                (when (ik_unfeasible_grasp ?x)(not (ik_unfeasible_grasp ?x)))
	                	)
	                )	    
			    )	
	)

	;; push along direction 3
	(:action push_dir3
		:parameters (?o - obj)
		:precondition 	(and	
				 			
				 			;; if there no exist objects that block ?o in all the directions
				 			(not (and
				 			;(exists (?x - obj)(block_dir1 ?x ?o))
				 			;(exists (?x - obj)(block_dir2 ?x ?o))
				 			(exists (?x - obj)(block_dir3 ?x ?o))
				 			;(exists (?x - obj)(block_dir4 ?x ?o))
				 			))

				 			;;if there are no objects above it
				 			(not (exists (?x - obj)(on ?x ?o)))

				 			;;if the object is not above to anyother
				 			(not (exists (?x - obj)(on ?o ?x))) ; this could be easily done adding a predicate to say if an object is on the table or not.

				 			(not (ik_unfeasible_dir3 ?o))
						)
			     
		;; effects: free "o" and take off all the block predicates that have 
		;; something to do with it. Also free all the object that have no more blocking objects  
		:effect (and
				    (forall (?x - obj)
				    	(and

				    	;;; eliminate the blocking predicate to all the objects that are adjacent to "o"	
				    	(when (block_dir1 ?o ?x) (not (block_dir1 ?o ?x)))
					    (when (block_dir2 ?o ?x) (not (block_dir2 ?o ?x)))
					    (when (block_dir3 ?o ?x) (not (block_dir3 ?o ?x)))
					    (when (block_dir4 ?o ?x) (not (block_dir4 ?o ?x)))

					    ; eliminate all the others blocking conditions - the others objects blocking condition on ?o
					    (when (block_dir1 ?x ?o) (not (block_dir1 ?x ?o)))
					    (when (block_dir2 ?x ?o) (not (block_dir2 ?x ?o)))
					    (when (block_dir3 ?x ?o) (not (block_dir3 ?x ?o)))
					    (when (block_dir4 ?x ?o) (not (block_dir4 ?x ?o)))

	  					; update the block_grasp predicates of all the objects 
	  					; which cannot be grasped because of ?o 
 	  					(when (block_grasp ?o ?x) (not (block_grasp ?o ?x)))
 	  					(when (block_grasp ?x ?o) (not (block_grasp ?x ?o)))

 	  					; update the ik_unfeasible_grasp, we consider that after a pushing
 	  					; if the object before couldn't be grasped, now it :constants
						(when (ik_unfeasible_grasp ?o) (not (ik_unfeasible_grasp ?o))) 	  					
					    )
				    )
	  				; remove all the geometric constraints
	  				(forall (?x - obj)
	  					(and
						(when (ik_unfeasible_dir1 ?x)(not (ik_unfeasible_dir1 ?x)))
		                (when (ik_unfeasible_dir2 ?x)(not (ik_unfeasible_dir2 ?x)))
		                (when (ik_unfeasible_dir3 ?x)(not (ik_unfeasible_dir3 ?x)))
		                (when (ik_unfeasible_dir4 ?x)(not (ik_unfeasible_dir4 ?x)))
		                (when (ik_unfeasible_grasp ?x)(not (ik_unfeasible_grasp ?x)))
	                	)
	                )	    
			    )	
	)

	;; push along direction 4
	(:action push_dir4
		:parameters (?o - obj)
		:precondition 	(and	
				 			
				 			;; if there no exist objects that block ?o in all the directions
				 			(not (and
				 			;(exists (?x - obj)(block_dir1 ?x ?o))
				 			;(exists (?x - obj)(block_dir2 ?x ?o))
				 			;(exists (?x - obj)(block_dir3 ?x ?o))
				 			(exists (?x - obj)(block_dir4 ?x ?o))
				 			))

				 			;;if there are no objects above it
				 			(not (exists (?x - obj)(on ?x ?o)))

				 			;;if the object is not above to anyother
				 			(not (exists (?x - obj)(on ?o ?x))) ; this could be easily done adding a predicate to say if an object is on the table or not.

				 			(not (ik_unfeasible_dir4 ?o))
						)
			     
		;; effects: free "o" and take off all the block predicates that have 
		;; something to do with it. Also free all the object that have no more blocking objects  
		:effect (and
			    	
				    (forall (?x - obj)
				    	(and

				    	;;; eliminate the blocking predicate to all the objects that are adjacent to "o"	
				    	(when (block_dir1 ?o ?x) (not (block_dir1 ?o ?x)))
					    (when (block_dir2 ?o ?x) (not (block_dir2 ?o ?x)))
					    (when (block_dir3 ?o ?x) (not (block_dir3 ?o ?x)))
					    (when (block_dir4 ?o ?x) (not (block_dir4 ?o ?x)))

					    ; eliminate all the others blocking conditions - the others objects blocking condition on ?o
					    (when (block_dir1 ?x ?o) (not (block_dir1 ?x ?o)))
					    (when (block_dir2 ?x ?o) (not (block_dir2 ?x ?o)))
					    (when (block_dir3 ?x ?o) (not (block_dir3 ?x ?o)))
					    (when (block_dir4 ?x ?o) (not (block_dir4 ?x ?o)))

	  					; update the block_grasp predicates of all the objects 
	  					; which cannot be grasped because of ?o 
 	  					(when (block_grasp ?o ?x) (not (block_grasp ?o ?x)))
 	  					(when (block_grasp ?x ?o) (not (block_grasp ?x ?o)))

 	  					; remove all the geometric constraints
 	  					(when (ik_unfeasible_dir1 ?x)(not (ik_unfeasible_dir1 ?x)))
		                (when (ik_unfeasible_dir2 ?x)(not (ik_unfeasible_dir2 ?x)))
		                (when (ik_unfeasible_dir3 ?x)(not (ik_unfeasible_dir3 ?x)))
		                (when (ik_unfeasible_dir4 ?x)(not (ik_unfeasible_dir4 ?x)))
		                (when (ik_unfeasible_grasp ?x)(not (ik_unfeasible_grasp ?x)))
					    )
				    )	    
			    )	
	)

	(:action grasp
	  :parameters (?o - obj)
	  :precondition (and
	  			    		(not (exists (?x - obj)(on ?x ?o)))
			  			    (not (exists (?x - obj)(block_grasp ?x ?o)))
			  			   ;; (graspable ?o)
			  			   	(not (ik_unfeasible_grasp ?o))

	  			    )
	  :effect   (and
	  				;object grasped predicate
	  				(grasped ?o)

	  				; all the predicates that have something to do with the object ?o are modified

	  				;all the object that had on themselves the object ?o 
	  				(forall (?x - obj)
	  					(when (on ?o ?x) (not (on ?o ?x)))
	  				)

	  				; update the block_grasp predicates of all the objects 
	  				; which cannot be grasped because of ?o 
	  				(forall (?x - obj)
	  					(when (block_grasp ?o ?x) (not (block_grasp ?o ?x)))
	  				)

	  				; update the blocks predicates of all the objects 
	  				; which cannot be pushed because of ?o 

	  				(forall (?x - obj)
	  					(and
	  					(when (block_dir1 ?o ?x) (not (block_dir1 ?o ?x)))
					    (when (block_dir2 ?o ?x) (not (block_dir2 ?o ?x)))
					    (when (block_dir3 ?o ?x) (not (block_dir3 ?o ?x)))
					    (when (block_dir4 ?o ?x) (not (block_dir4 ?o ?x)))
	  					)
	  				)

	  				; remove all the geometric constraints
	  				(forall (?x - obj)
	  					(and
						(when (ik_unfeasible_dir1 ?x)(not (ik_unfeasible_dir1 ?x)))
		                (when (ik_unfeasible_dir2 ?x)(not (ik_unfeasible_dir2 ?x)))
		                (when (ik_unfeasible_dir3 ?x)(not (ik_unfeasible_dir3 ?x)))
		                (when (ik_unfeasible_dir4 ?x)(not (ik_unfeasible_dir4 ?x)))
		                (when (ik_unfeasible_grasp ?x)(not (ik_unfeasible_grasp ?x)))
	                	)
	                )
	  			)
	)

)
