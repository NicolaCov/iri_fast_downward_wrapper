(define (problem problem_example_full)
(:domain domain_example_full)
(:objects 
o0 - obj 
o1 - obj 
)
(:init 
(= (total-cost) 0 )
(block_dir3 o1 o0 )
(block_dir4 o1 o0 )
(block_dir1 o0 o1 )
(block_dir2 o0 o1 )
)
(:goal 
(and                     (not (exists (?x - obj)(not (removed ?x))))
                     )
)
(:metric minimize (total-cost))
)