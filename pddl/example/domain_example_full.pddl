(define (domain domain_example_full)
(:requirements :adl :existential-preconditions :universal-preconditions :action-costs )
(:types obj direction )
(:predicates 
(block_dir ?o1 - obj ?o2 - obj ?d - direction )
(on ?o1 - obj ?o2 - obj )
(block_grasp ?o1 - obj ?o2 - obj )
(removed ?o - obj )
(ik_unfeasible_dir ?o - obj ?d - direction )
(ik_unfeasible_grasp ?o - obj )
)
(:functions (total-cost) - number)
(:action grasp-o0
:parameters ()
:precondition (and
(not (exists (?x - obj)(on ?x o0)))
(not (exists (?x - obj)(block_grasp ?x o0)))
(not (ik_unfeasible_grasp o0))
)
:effect (and
(removed o0)
(forall (?x - obj)
(when (on o0 ?x) (not (on o0 ?x)))
)
(forall (?x - obj)
(when (block_grasp o0 ?x) (not (block_grasp o0 ?x)))
)
(forall (?x - obj)
(and
(forall (?d - direction)
(when (block_dir o0 ?x ?d)(not (block_dir o0 ?x ?d))))))
(forall (?x - obj)
(and 
 (forall (?d - direction)
(when (ik_unfeasible_dir ?x ?d)(not (ik_unfeasible_dir ?x ?d)))
)
(when (ik_unfeasible_grasp ?x)(not (ik_unfeasible_grasp ?x)))
)
)
(increase (total-cost) 10)
)
)
(:action grasp-o1
:parameters ()
:precondition (and
(not (exists (?x - obj)(on ?x o1)))
(not (exists (?x - obj)(block_grasp ?x o1)))
(not (ik_unfeasible_grasp o1))
)
:effect (and
(removed o1)
(forall (?x - obj)
(when (on o1 ?x) (not (on o1 ?x)))
)
(forall (?x - obj)
(when (block_grasp o1 ?x) (not (block_grasp o1 ?x)))
)
(forall (?x - obj)
(and
(forall (?d - direction)
(when (block_dir o1 ?x ?d)(not (block_dir o1 ?x ?d))))))
(forall (?x - obj)
(and 
 (forall (?d - direction)
(when (ik_unfeasible_dir ?x ?d)(not (ik_unfeasible_dir ?x ?d)))
)
(when (ik_unfeasible_grasp ?x)(not (ik_unfeasible_grasp ?x)))
)
)
(increase (total-cost) 5)
)
)
)