# iri_fast_downward_wrapper #

This ros package allows to get a plan of planning problem by using the Fast Downward algorithm. Until now it only can handle symbolic predicates since for my problem they were enough. 

It works through a service.
IMPORTANT: the domain is built offline by hand by the user and its domain filed has to be the same of the file name (e.g. (domain domain_example) in domain_example.pddl). 

To make it easy put the domain file inside the pddl folder, then you can view the problem_example.pddl to verify if everything was correct. Remember to don't call the files "domain" and "problem", the Fst Dowanward planner gives problem in the parsing phase. Just call it with whatever other names. You can choose the domain and problem names in the launch file. 