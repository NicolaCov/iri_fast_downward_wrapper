// Copyright (C) 2010-2011 Institut de Robotica i Informatica Industrial, CSIC-UPC.
// Author 
// All rights reserved.
//
// This file is part of iri-ros-pkg
// iri-ros-pkg is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script from the 
// iri_ros_scripts. Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. Please
// refer to the IRI wiki page for more information:
// http://wikiri.upc.es/index.php/Robotics_Lab

#ifndef _fast_downward_wrapper_alg_h_
#define _fast_downward_wrapper_alg_h_

#include <iri_fast_downward_wrapper/FastDownwardWrapperConfig.h>

//include fast_downward_wrapper_alg main library
#include <fast_downward_wrapper.h> 

const std::string FAST_DOWNWARD_FOLDER = "~/FastDownward";
const std::string PROBLEM_NAME = "problem_";
const std::string DOMAIN_NAME = "domain_";
const std::string WORKING_FOLDER = "/home/ncovallero/iri-lab/iri_ws/src/iri_fast_downward_wrapper/pddl";
const std::string HEURISTIC = "h=blind()";
const std::string SEARCH_METHOD = "eager_greedy(h,preferred=h)";

/**
 * \brief IRI ROS Specific Driver Class
 *
 *
 */
class FastDownwardWrapperAlgorithm
{
  protected:
   /**
    * \brief define config type
    *
    * Define a Config type with the FastDownwardWrapperConfig. All driver implementations
    * will then use the same variable type Config.
    */
    pthread_mutex_t access_;    

    // private attributes and methods

    /**
     * Wrapper of the Fast Downward Planner
     */
    CFast_Downward_Wrapper fdw;

  public:
   /**
    * \brief define config type
    *
    * Define a Config type with the FastDownwardWrapperConfig. All driver implementations
    * will then use the same variable type Config.
    */
    typedef iri_fast_downward_wrapper::FastDownwardWrapperConfig Config;

   /**
    * \brief config variable
    *
    * This variable has all the driver parameters defined in the cfg config file.
    * Is updated everytime function config_update() is called.
    */
    Config config_;

   /**
    * \brief constructor
    *
    * In this constructor parameters related to the specific driver can be
    * initalized. Those parameters can be also set in the openDriver() function.
    * Attributes from the main node driver class IriBaseDriver such as loop_rate,
    * may be also overload here.
    */
    FastDownwardWrapperAlgorithm(void);

   /**
    * \brief Lock Algorithm
    *
    * Locks access to the Algorithm class
    */
    void lock(void) { pthread_mutex_lock(&this->access_); };

   /**
    * \brief Unlock Algorithm
    *
    * Unlocks access to the Algorithm class
    */
    void unlock(void) { pthread_mutex_unlock(&this->access_); };

   /**
    * \brief Tries Access to Algorithm
    *
    * Tries access to Algorithm
    * 
    * \return true if the lock was adquired, false otherwise
    */
    bool try_enter(void) 
    { 
      if(pthread_mutex_trylock(&this->access_)==0)
        return true;
      else
        return false;
    };

   /**
    * \brief config update
    *
    * In this function the driver parameters must be updated with the input
    * config variable. Then the new configuration state will be stored in the 
    * Config attribute.
    *
    * \param new_cfg the new driver configuration state
    *
    * \param level level in which the update is taken place
    */
    void config_update(Config& config, uint32_t level=0);

    // here define all fast_downward_wrapper_alg interface methods to retrieve and set
    // the driver parameters

   /**
    * \brief Destructor
    *
    * This destructor is called when the object is about to be destroyed.
    *
    */
    ~FastDownwardWrapperAlgorithm(void);

    void setFastDownwardFolder(std::string planner_folder);

    void setWorkingFolder(std::string working_folder);

    /**
     * @details Set the name of the domain file [without the ".pddl" extension]. IMPORTANT
     * the domain_file_name has to be both the name of file and the name of the domain (domain domain_file_name)
     * 
     * @param domain_file_name 
     */
    void setDomainPDDLFile(std::string domain_file_name);

    /**
     * @details Set the name of the problem file [without the ".pddl" extension]
     * 
     * @param problem_file_name 
     */
    void setProblemPDDLFile(std::string problem_file_name);

    /**
     * @brief Set the objects of the problem
     * @details Set the objects of the problem
     * 
     * @param objects 
     */
    void setObjects(Fast_Downward_Wrapper::Objects objects);

    /**
     * @details Set symbolic predicates
     * 
     * @param sym_predicates 
     */
    void setSymbolicPredicates(Fast_Downward_Wrapper::SymbolicPredicates sym_predicates);

    /**
     * @brief Set the goal
     * @details Set the goal of the problem. Due to big variety of possible expression 
     * in this field, the goal expression has to be specified by the user as an unique string.
     * 
     * @param goal 
     */
    void setGoal(std::string goal);

    /**
     * @brief Set heuristic method
     * @details The heuristic method is jsut a string which is the same that will be put in the command
     * to call the planner. See the example.
     * 
     * @param heuristic 
     */
    void setHeuristic(std::string heuristic);

    /**
     * @brief Set search method
     * @details The search method is jsut a string which is the same that will be put in the command
     * to call the planner. See the example.
     * 
     * @param search
     */
    void setSearch(std::string search);

    /**
     * @brief Compute the plan
     * @details Call the planner and save it.
     * 
     * @return True if there is a solution
     */
    bool computePlan();

    /**
     * @brief Print in the terminal the obtained plan.
     */
    void printPlan();

    /**
     * @brief Get the plan
     * @details Get the plan
     * @return the plan
     */
    Fast_Downward_Wrapper::Plan getPlan();

    /**
     * @brief Add total cost to the domain description if cost = true
     * 
     * @param cost 
     */
    void addTotalCost(bool cost);

    void setTypes(std::vector<std::string> types);

    void setDomainSymbolicPredicates(Fast_Downward_Wrapper::DomainSymbolicPredicates domain_symbolic_predicates);

    void setDomainActions(Fast_Downward_Wrapper::DomainActions domain_actions);

    void writeDomainPDDLFile();
};

#endif
